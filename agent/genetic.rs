use super::optimization::*;
use super::math::*;
use rand::*;
use rand::prelude::*;
use derivative::*;
use std::cmp::*;

/// A genetic algorithm, computes a new population by taking the best candidates
/// from the previous generation and mixing their parameters.
#[derive(Clone, Debug, Derivative)]
#[derivative(Default)]
pub struct GeneticAlgorithm {
   #[derivative(Default(value="Vector::zeros(0)"))]
   pub mean: Vector,
   #[derivative(Default(value="0.2"))]
   pub truncate: f32,
   #[derivative(Default(value="1.0"))]
   pub standard_deviation: f32,
}

impl Strategy for GeneticAlgorithm {
   fn solve<'a>(&mut self, individuals: &mut [Individual]) {
      let mut rng = rand::thread_rng();

      let k = ((individuals.len() as f32 * self.truncate) as usize).max(1);

      // Get the indices of the k best individuals
      let best = k_best(individuals, k);
      self.mean = individuals[best[0]].parameters.clone();

      // TODO: avoid this; borrowck sucks sometimes
      let _best: Vec<_> = best.iter().map(|&i| individuals[i].parameters.clone()).collect();
      let dist = rand::distributions::Normal::new(0.0, self.standard_deviation as f64);

      for (idx, i) in individuals.iter_mut().enumerate() {
         if best.contains(&idx) { continue }

         // Cross two random individuals from the best k
         let a = rng.gen_range(0, k);
         let b = rng.gen_range(0, k);
         i.parameters = Vector::from_fn(_best[a].len(), |idx, _|
            if rng.gen_bool(0.5) { _best[a][idx] } else { _best[b][idx] }
         );

         i.parameters.apply(|v| v + dist.sample(&mut rng) as f32);
      }
   }

   fn best(&self) -> &Vector { &self.mean }
}
