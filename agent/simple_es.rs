use super::math::*;
use super::optimization::*;
use serde::*;
use rand::prelude::*;

/// Evolutionary strategy where the mean for the next generation is the
/// average of the previous generation's individuals weighted by some
/// function.
#[derive(Clone, Debug)]
pub struct WeightedES {
   pub mean: Vector,
   pub standard_deviation: f32,
   pub learning_rate: f32,
   pub weight: Weight,
}

/// Describes how to weight individuals when combining parameters to a new mean.
#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum Weight {
   /// Pick the individual with the highest fitness at each step
   Best,
   /// Weight the top fraction of individuals by their rank.
   /// Lower values give higher precedent to the best solution
   /// in a particular generation, possibly speeding up training
   Rank(f32),
   /// Weight the top fraction of individuals by their fitness
   Fitness(f32),
}

impl Strategy for WeightedES {
   fn solve<'a>(&mut self, individuals: &mut [Individual]) {
      if individuals.len() == 0 { return }
      let p = individuals.len() as f32;

      // Update the mean with a weighted average of the individuals
      let mean = match self.weight {
         Weight::Best => {
            // Take the best individual
            let idx = best(individuals);
            individuals[idx].parameters.clone()
         }
         Weight::Rank(k) => {
            // Weight individuals by rank
            let k = (p * k) as usize;
            let best = k_best(individuals, k);
            let mut v = Vector::from_fn(k, |idx, _| (best.len() - idx + 1) as f32);
            v /= v.sum();

            best.iter().zip(v.as_slice())
               .map(|(&n, &w)| &individuals[n].parameters * w)
               .sum()
         }
         Weight::Fitness(k) => {
            // Weight individuals by relative fitness
            let k = (p * k) as usize;
            let best = k_best(individuals, k);

            let w = Vector::from_fn(k, |idx, _| individuals[best[idx]].fitness);
            let w_min = w.min();
            let w = w.map(|x| x - w_min) / w.sum();

            best.iter().zip(w.as_slice())
               .map(|(&n, &w)| &individuals[n].parameters * w)
               .sum()
         }
      };

      // Move the mean toward the calculated value
      if self.mean.len() == 0 {
         self.mean = mean;
      } else {
         self.mean = &self.mean * (1.0 - self.learning_rate) + mean * self.learning_rate;
      }

      // Generate a normally distributed population with the current mean and
      // standard deviation.
      let mut rng = rand::thread_rng();
      let dist = rand::distributions::Normal::new(0.0, self.standard_deviation as f64);
      for i in individuals {
         i.parameters.clone_from(&self.mean);
         i.parameters.apply(|v| v + dist.sample(&mut rng) as f32);
      }
   }

   fn best(&self) -> &Vector { &self.mean }
}
