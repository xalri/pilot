use std::ops::*;
use serde::*;
use super::math::*;

/// A sequence of layers
#[derive(Clone, Debug)]
pub struct Network {
   pub inputs: usize,
   pub outputs: usize,
   pub params: Vector,
   pub layers: Vec<Layer>,
   pub output: Vec<f32>,
   pub tmp: Vec<f32>,
}

/// A layer of a neural network
#[derive(Clone, Debug)]
pub enum Layer {
   /// A fully connected layer
   Linear{ nodes: Vec<Range<usize>> },
   /// A linear offset
   Bias{ nodes: Range<usize> },
   /// An activation layer
   Activation(Activation),
   /// A normalization layer
   Normalize,
}

/// An activation function
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Activation { Relu, Tanh, None }

impl Network {
   /// Load a network from a RON descriptor
   pub fn from_descriptor(s: &str) -> Network {
      let descriptor: NetworkDescriptor = ron::de::from_str(s).unwrap();
      descriptor.build()
   }

   /// Forward data through the network
   pub fn forward(&mut self, input: &[f32]) {
      assert_eq!(input.len(), self.inputs);

      self.output.clear();
      self.output.extend_from_slice(input);

      for layer in &self.layers {
         self.tmp.clear();
         self.tmp.append(&mut self.output);
         layer.forward(self.params.as_slice(), &self.tmp, &mut self.output);
      }

      assert_eq!(self.output.len(), self.outputs);
   }
}

impl Layer {
   /// Propagate values forward through the layer.
   pub fn forward(&self, params: &[f32], input: &[f32], output: &mut Vec<f32>) {
      match self {
         Layer::Linear{ nodes } => {
            for weights in nodes {
               let sum: f32 = input.iter().zip(&params[weights.clone()])
                  .map(|(value, weight)| value * weight)
                  .sum();
               output.push(sum / input.len() as f32);
            }
         },
         Layer::Bias{ nodes } => {
            output.extend_from_slice(input);
            for (value, bias) in output.iter_mut().zip(&params[nodes.clone()]) {
               *value += bias;
            }
         },
         Layer::Activation(x) => {
            output.extend_from_slice(input);
            for o in output { *o = x.apply(*o) }
         },
         Layer::Normalize => {
            output.extend_from_slice(input);
            let sum: f32 = input.iter().sum();
            for o in output { *o /= sum }
         }
      }
   }
}

impl Activation {
   /// Apply the activation function
   pub fn apply(&self, value: f32) -> f32 {
      match *self {
         Activation::Relu => value.max(0.0),
         Activation::Tanh => value.tanh(),
         Activation::None => value,
      }
   }
}

/// Describes the topology of a neural network
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct NetworkDescriptor {
   pub inputs: usize,
   pub layers: Vec<LayerDescriptor>,
   pub params: Option<Vec<f32>>,
}

/// Describes a layer of a neural network
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum LayerDescriptor {
   Linear{ nodes: usize },
   Bias,
   Activation(Activation),
   Normalize,
}

impl NetworkDescriptor {
   /// Create a network from the descriptor
   pub fn build(&self) -> Network {
      let mut n = Network {
         params: Vector::zeros(0),
         inputs: self.inputs,
         outputs: self.inputs,
         layers: vec![],
         output: vec![],
         tmp: vec![],
      };

      for l in &self.layers {
         match l {
            LayerDescriptor::Linear{ nodes } => {
               let _nodes = (0..*nodes).map(|_| alloc(&mut n.params, n.outputs)).collect();
               n.layers.push(Layer::Linear { nodes: _nodes });
               n.outputs = *nodes;
            },
            LayerDescriptor::Bias => {
               n.layers.push(Layer::Bias{
                  nodes: alloc(&mut n.params, n.outputs)
               })
            }
            LayerDescriptor::Activation(a) => {
               n.layers.push(Layer::Activation(*a));
            }
            LayerDescriptor::Normalize => {
               n.layers.push(Layer::Normalize);
            }
         }
      }
      if let Some(ref p) = self.params {
         assert_eq!(n.params.len(), p.len());
         n.params = Vector::from_vec(p.clone())
      }
      n
   }
}
