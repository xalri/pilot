use super::math::*;
use lazysort::*;
use std::cmp::*;

/// A black-box optimization strategy. Given a set of individuals, attempts
/// to modify their parameters to maximize fitness.
pub trait Strategy: std::fmt::Debug {
   /// Update individuals
   fn solve<'a>(&mut self, individuals: &mut [Individual]);

   /// Get the current best parameter vector
   fn best(&self) -> &Vector;
}

/// The parameters of a candidate solution to a problem.
#[derive(Debug, Clone)]
pub struct Individual {
   pub parameters: Vector,
   pub fitness: f32,
}

/// Get the index of the individual with the highest fitness
pub fn best<'a>(individuals: &[Individual]) -> usize {
   individuals.iter().enumerate()
      .max_by(|(_,a), (_,b)| a.cmp(b)).unwrap().0
}

/// Get the indices of the k best individuals
pub fn k_best(individuals: &[Individual], k: usize) -> Vec<usize> {
   individuals.iter().enumerate()
      .sorted_by(|(_, a), (_, b)| b.cmp(a)) // descending partial sort
      .take(k)
      .map(|(idx, _)| idx)
      .collect::<Vec<usize>>()
}

impl PartialEq for Individual {
   fn eq(&self, rhs: &Self) -> bool {
      let a = self.fitness;
      let b = rhs.fitness;

      (!a.is_finite() && !b.is_finite()) || a == b
   }
}
impl Eq for Individual { }

impl PartialOrd for Individual {
   fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
      Some(self.cmp(rhs))
   }
}

impl Ord for Individual {
   fn cmp(&self, rhs: &Self) -> Ordering {
      let a = self.fitness;
      let b = rhs.fitness;

      if !a.is_finite() && !b.is_finite() { Ordering::Equal }
      else if !a.is_finite() { Ordering::Less }
      else if !b.is_finite() { Ordering::Greater }
      else {
         a.partial_cmp(&b).unwrap()
      }
   }
}
