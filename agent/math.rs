pub type Vector = nalgebra::DVector<f32>;
pub type Matrix = nalgebra::DMatrix<f32>;

use rand::distributions::StandardNormal;
use rand::prelude::*;
use std::ops::Range;

/// Allocate a new range in a dynamic vector
pub fn alloc(params: &mut Vector, size: usize) -> Range<usize> {
   let start = params.len();
   *params = params.clone().resize_vertically(start + size, 0.0);
   start..params.len()
}

/// Multivariate normal distribution
pub struct MultivariateNormal {
   pub mean: Vector,
   pub transform: Matrix,
   pub normal: StandardNormal,
}

impl MultivariateNormal {
   /// Create a new multivariate normal distribution with the given mean & covariance
   pub fn new(mean: Vector, covariance: Matrix) -> Self {
      let decomp = nalgebra::SymmetricEigen::new(covariance);
      let diag = decomp.eigenvalues.map(|v| v.abs().sqrt());
      let transform = decomp.eigenvectors * Matrix::from_diagonal(&diag);
      MultivariateNormal {
         mean,
         transform,
         normal: StandardNormal,
      }
   }
}

impl Distribution<Vector> for MultivariateNormal {
   fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Vector {
      let vec = Vector::from_fn(self.mean.data.len(), |_,_| self.normal.sample(rng) as f32);
      &self.mean + &self.transform * vec
   }
}
