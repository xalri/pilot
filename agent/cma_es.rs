//! Covariant matrix adaptation evolution strategy

use super::math::*;
use super::optimization::*;
use rand::prelude::*;

/*
https://arxiv.org/pdf/1604.00772v1.pdf

Notes:
- mu -> parents
- lambda -> offspring (population size)
- Goal is for covar matrix to approximate contour lines of objective
   - equiv to approximating inverse hessian matrix
     for convex-quadratic functions (?)
 - Condition number ~ rate at which a change in the input changes the output
 - ||A|| = largest eigenvalue of A (no idea why)
 - normal distribution has the largest distribution of all
   distributions in R^n
 - soo.. we know how to sample a value, next step is to find
   the mean & covar for the next step
 - New mean is a weighted average of the previous population,
   usually weighted by ranking
   - weights sum to 1
   - all weights are positive
   - oh lol that gives me an idea for normal weighted ES:
     - no need to sum the entire matrices: calculate the
       normalised weights first, then multiply & sum in one step
   - paper suggests w = 1/mu
   - can we weight by fitness instead?
     - this has no truncation? good thing or bad thing?
      - so that's a parameter to play with, including in normal
        weighted ES
   - mu_eff = 1 / (sum of squared weights)
    -> that is a thing.. sure I've seen before in stats.
    - so, bunch of values that some to 1. square them all & sum,
      what do you get? how does the output correspond to input
      some measure of how the weights vary
    - between 1 and mu
    - equal to mu for all equal weights, smaller when weights vary more
    - paper says mu_eff ~ population / 4 is good, why? test?
      - something maybe related: to estimate covar matrix s.t. cond(C) < 10
        requires sample size of > 4n

    - new mean is prev mean + (learning rate * sum of weighted difference from mean)
      - (learning rate usually 1) -> new mean is weighted sum of prev mean
      - learning rate < 1 can be better for noisy functions

   - Estimating covar matrix:
    - emepirical covar matrix = 1/mu * weighted sum of  (params - mean) * transpose
    -> diagonal of squared weighted sum


  - On rates of change:
   - expected parameter change per sampled point
   - cma-es controls change rate of mean, covar, and step size

*/

use serde::*;

/// Evolutionary strategy with covariant matrix adaptation
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CMA {
   pub mean: Vector,
   pub covar: Matrix,
   pub inv_transform: Matrix,
   pub path_c: Vector,
   pub path_s: Vector,
   pub step_size: f32,
   pub generation: usize,
   pub fitness: f32,
   // The number of generations since the covariance matrix was updated
   //pub prev_covar_update: usize,
}

impl CMA {
   pub fn new(n: usize) -> CMA {
      CMA {
         mean: Vector::zeros(n),
         path_c: Vector::zeros(n),
         path_s: Vector::zeros(n),
         covar: Matrix::identity(n, n),
         inv_transform: Matrix::identity(n, n),
         step_size: 1.0,
         generation: 0,
         fitness: 0.0,
      }
   }
}

impl Strategy for CMA {
   fn solve<'a>(&mut self, individuals: &mut [Individual]) {
      if individuals.is_empty() { return }

      for i in &mut *individuals { if !i.fitness.is_finite() { i.fitness = 0.0; } }
      self.fitness = individuals.iter().map(|i| i.fitness).sum();
      self.fitness /= individuals.len() as f32;
      self.generation += 1;

      let n = individuals[0].parameters.len() as f32; // Number of parameters

      // Get the subset of the population which contribute.
      let λ = individuals.len(); // Total population size
      let μ = (λ as f32 * 0.5) as usize; // Number of parents
      let best = k_best(individuals, μ); // Indices of parents

      // Calculate recombination weights
      // TODO: negative weights kill everything? ._.
      let mut w = Vector::from_fn(μ, |idx, _| individuals[best[idx]].fitness.max(0.00001));
      w /= w.sum();

      // Calculate various parameters:

      // Variance effective selection mass, a measure of how close in fitness
      // the top μ individuals are.
      let mu_eff = 1.0 / (w.map(|x| x * x).sum());
      // Evolution path smoothing factor
      let cc = (4.0 + mu_eff / n) / (n + 4.0 + 2.0 * mu_eff / n);
      // Learning rate for rank-1 update
      let c_1 = 2.0 / ((n + 1.3).powi(2) + mu_eff);
      // Learning rate for rank-mu update
      let c_mu = 2.0 * (mu_eff - 2.0 + 1.0 / mu_eff) / ((n + 2.0).powi(2) + mu_eff);
      let c_mu = c_mu.min(1.0 - c_1);
      // Step size adaptation
      let c_step = (mu_eff + 2.0) / (n + mu_eff + 5.0);
      let d_step = 1.0 + 2.0 * (((mu_eff - 1.0) / (n + 1.0)).sqrt() - 1.0).max(0.0) + c_step;

      // Calculate the new mean
      let mean = best.iter().enumerate()
         .map(|(i, &parent)| w[i] * &individuals[parent].parameters)
         .sum();
      let mean_diff = &mean - &self.mean;

      let x = (c_step * (2.0 - c_step) * mu_eff).sqrt(); // Normalization constant
      // transform makes expected length indepenedent from it's rotation
      // -> path length is normally distributed with covar = identity
      self.path_s = (1.0 - c_step) * &self.path_s + (x / self.step_size) * (&self.inv_transform * &mean_diff);

      // Update covariance matrix evolution path, used to stretch the distribution in the
      // direction the mean is moving:
      self.path_c = (1.0 - cc) * &self.path_c +
         (cc * (2.0 - cc) * mu_eff).sqrt() * &mean_diff / self.step_size;

      // Update covariance matrix:
      self.covar *= 1.0 - c_1 - c_mu;
      self.covar += c_1 * (&self.path_c * self.path_c.transpose());
      self.covar += c_mu * best.iter().enumerate()
         .map(|(n, &idx)| {
            let y = (&individuals[idx].parameters - &self.mean) / self.step_size;
            w[n] * (&y * y.transpose())
         })
         .sum::<Matrix>();

      // Update step size
      // expected value of euclidean norm of x ~ N(0, I)
      let e = (n).sqrt() * (1.0 - 1.0 / (4.0 * n) + 1.0 / (21.0 * (n).powi(2)));
      // TODO: This was commented?
      self.step_size *= ((c_step / d_step) * ((self.path_s.norm()) / e - 1.0)).exp();

      // Update mean
      self.mean = mean;

      assert!(self.mean[0].is_finite());
      assert!(self.covar[(0,0)].is_finite());

      for v in &mut self.mean { if !v.is_finite() { *v = 0.0 } }
      for v in &mut self.covar { if !v.is_finite() { *v = 0.0 } }
      for v in &mut self.path_c { if !v.is_finite() { *v = 0.0 } }
      for v in &mut self.path_s { if !v.is_finite() { *v = 0.0 } }

      // Generate a new population with the updated mean & covariance
      let dist = MultivariateNormal::new(Vector::zeros(self.mean.len()), self.covar.clone());
      let mut rng = rand::thread_rng();
      for i in individuals { i.parameters = &self.mean + self.step_size * &dist.sample(&mut rng); }
      self.inv_transform = dist.transform.try_inverse().unwrap();
   }

   fn best(&self) -> &Vector { &self.mean }
}
