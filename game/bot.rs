use super::plane::*;
use crate::agent::nn::*;
use esc::storage::*;
use esc::geom::*;
use esc::render::{DebugLines, has_facade};

/// The target for a navigating agent
#[derive(Default, Copy, Clone)]
pub struct Target {
   pub position: Vec2,
   pub velocity: Vec2,
   pub forward: Vec2,
}

/// A measure of how well the agent flies, used for training.
#[derive(Default, Copy, Clone)]
pub struct Fitness {
   pub value: f32,
}

/// A navigating agent using a neural network.
#[derive(Debug, Clone)]
pub struct NeuralNavigator {
   pub network: Network,
   pub memory: Vec<f32>,
   pub fitness: f32,
}

pub fn neural_navigation(
   subs: &mut Subscriptions,
   control: &mut Store<PlaneControl>,
   navigator: &mut Store<NeuralNavigator>,
   flight: &Store<Flight>,
   target: &Store<Target>,
   transform: &Store<Transform>,
   velocity: &Store<Velocity>,

   lines: &mut DebugLines,
   broadphase: &mut Broadphase,
   c_poly: &Store<CollisionPoly>,
   c_mask: &Store<CollisionMask>,
) {
   let iter = subs.iter::<(Flight, PlaneControl, NeuralNavigator, Target, Transform)>
   ((flight, control, navigator, target, transform));

   for e in iter {
      control[e].turn = 0.0;
      control[e].throttle_diff = 0.0;

      let target_pos = target[e].position - transform[e].position;
      let target_dir = target_pos.normalize();
      let target_dist = target_pos.length() / 1000.0;
      let forward = target[e].forward;
      let vel = velocity[e].linear.normalize();
      let facing = Vec2::from_angle(transform[e].orientation);

      // Cast a ray into the world to give the bot vision
      let mut ray = |offset: f32| {
         let pos = transform[e].position;
         let dir = Vec2::from_angle(transform[e].orientation + offset);
         let dist = raycast(broadphase, c_mask, c_poly, transform, pos, dir, 0b10);
         let dist = clamp(dist, 50.0, 1000.0);
         if has_facade() {
            let b = pos + dir * dist;
            lines.lines.push((pos, b));
         }
         dist / 1000.0
      };

      navigator[e].memory.resize(10, 0.0);
      let input = [
         // 11
         target_dir.x, target_dir.y,
         forward.x, forward.y,
         facing.x, facing.y,
         vel.x, vel.y,
         target_dist,
         flight[e].throttle,
         flight[e].turn,

         // Allows the agent to control it's raycasts
         //ray(vec2(navigator[e].memory[3], navigator[e].memory[4]).angle()),
         
         ray(-1.7), ray(-0.7), ray(-0.05),
         ray(1.7), ray(0.7), ray(0.05),
         /*
         navigator[e].memory[0],
         navigator[e].memory[1],
         navigator[e].memory[2],
         navigator[e].memory[3],
         navigator[e].memory[4],
         navigator[e].memory[5],
         navigator[e].memory[6],
         navigator[e].memory[7],
         navigator[e].memory[8],
         navigator[e].memory[9],
         */
      ];

      //for i in input { assert!(i.is_finite()) }

      navigator[e].network.forward(&input);
      let output = &navigator[e].network.output;
      control[e].turn = wrap_angle(vec2(output[0], output[1]).angle());
      control[e].throttle_diff = output[2];
      navigator[e].memory = output.to_vec();
   }
}
