use serde::*;
use esc::storage::*;
use esc::resources::*;
use esc::geom::*;
use esc::render::*;
use esc::input::*;
use esc::error::*;

use super::plane::*;
use super::bot::*;

use super::scenarios::collect::*;
#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Protos(#[serde(default="Default::default")] Vec<String>);

#[derive(Default, Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct Entity {
   pub protos: Option<Protos>,
   pub transform: Option<Transform>,
   pub velocity: Option<Velocity>,
   pub mass: Option<Mass>,
   pub acceleration: Option<Acceleration>,
   pub collision_mask: Option<CollisionMask>,
   pub collision_poly: Option<CollisionPoly>,
   pub body: Option<RigidBody>,
   pub bounds: Option<Bounds>,
   pub flight: Option<Flight>,
   pub plane_anim: Option<PlaneAnim>,
   pub experience: Option<Experience>,
   pub params: Option<PlaneParams>,
   pub control: Option<PlaneControl>,
   pub emp: Option<Emp>,
   pub controller: Option<Controller>,
   pub textured: Option<Textured>,
   pub mesh: Option<Mesh<Vertex2>>,
   #[serde(flatten)]
   _rest: std::collections::BTreeMap<String, ron::Value>,
}

impl Entity {
   pub fn apply(&self, world: &mut Storage, e: usize) {
      if !self._rest.is_empty() {
         let mut fields = "".to_string();
         for r in self._rest.keys() { fields += r }
         panic!("Unknown fields: {}", fields);
      }

      if let Some(Protos(protos)) = &self.protos {
         for p in protos {
            log::debug!("Applying prototype {} {}", p, e);
            res::get::<Entity>(p).unwrap().apply(world, e)
         }
      }

      if let Some(v) = &self.transform { world.insert(e, v.clone()) }
      if let Some(v) = &self.velocity { world.insert(e, v.clone()) }
      if let Some(v) = &self.mass { world.insert(e, v.clone()) }
      if let Some(v) = &self.acceleration { world.insert(e, v.clone()) }
      if let Some(v) = &self.collision_mask { world.insert(e, v.clone()) }
      if let Some(v) = &self.collision_poly { world.insert(e, v.clone()) }
      if let Some(v) = &self.body { world.insert(e, v.clone()) }
      if let Some(v) = &self.bounds { world.insert(e, v.clone()) }
      if let Some(v) = &self.flight { world.insert(e, v.clone()) }
      if let Some(v) = &self.plane_anim { world.insert(e, v.clone()) }
      if let Some(v) = &self.experience { world.insert(e, v.clone()) }
      if let Some(v) = &self.params { world.insert(e, v.clone()) }
      if let Some(v) = &self.control { world.insert(e, v.clone()) }
      if let Some(v) = &self.emp { world.insert(e, v.clone()) }
      if let Some(v) = &self.controller { world.insert(e, v.clone()) }
      if let Some(v) = &self.textured { world.insert(e, v.clone()); }
      if let Some(v) = &self.mesh { world.insert(e, v.clone()) }
   }

   pub fn reify(&self, world: &mut Storage) -> usize {
      let e = world.create();
      self.apply(world, e);
      e
   }
}

impl Resource for Entity {
   type Descriptor = Entity;
   fn load(_: &dyn Source, entity: Self::Descriptor) -> Result<Self> {
      Ok(entity)
   }
}

pub fn input(s: &mut Storage) {
   s.run(&update_window_input_sources); // Window -> InputSource
   s.run(&map_controls); // InputSource -> Controller
   s.run(&player_plane_controls); // Controller -> PlaneControl
}

pub fn tick(s: &mut Storage) {
   s.run(&clear_debug_lines);

   s.run(&integrate_acceleration);
   s.run(&calculate_bounds);
   s.run(&update_broadphase);
   s.run(&find_pairs);
   s.run(&find_contacts);
   s.run(&solve_rigid_body_contacts);
   s.run(&plane_pre_collide);
   s.run(&solve_constraints);
   s.run(&plane_post_collide);

   s.run(&derive_plane_params);
   s.run(&fly);
   s.run(&update_plane_anim);
   s.run(&cycle_hue);

   s.run(&integrate_velocity);

   s.clean();
}

pub fn draw(s: &mut Storage) {
   s.run(&update_view_dimensions);
   s.run(&draw_meshes::<Textured>);
   s.run(&draw_colliders);
   s.run(&draw_collectables);
   s.run(&draw_lines);
}

pub fn cycle_hue(t: &mut Store<Textured>) {
    for (_, t) in t.iter_mut() {
       if !t.tint.is_empty() {
          t.tint[0].hsv.hue = wrap(t.tint[0].hsv.hue + 0.002, -0.5, 0.5);
       }
    }
 }
