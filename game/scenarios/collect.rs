use std::f32::consts::PI;
use rand::*;
use esc::geom::*;
use esc::storage::*;
use crate::game::plane::Flight;
use crate::game::bot::*;

/// An event sent when a game is over.
/// Learned parameters will be tweaked to give higher fitness.
#[derive(Clone, Copy, Debug)]
pub struct EndEpoch;

/// Distance at which points are collected
const COLLECT_DIST: f32 = 60.0;

/// The scenario itself
#[derive(Default)]
pub struct CollectionGame {
   /// Progress through the game in ticks.
   pub time: usize,
   /// A set of things to collect.
   pub targets: Vec<usize>,
   /// Dynamic obstacles for collectors to avoid.
   pub obstacles: Vec<usize>,
}

/// Something which collects things
#[derive(Default, Clone, Debug)]
pub struct Collector {
   /// The entity ID of the thing to collect
   pub target: Option<usize>,
   /// The total distance we've traveled toward collectables
   pub journey: f32,
   /// The previous position of the collector
   pub prev_pos: Option<Vec2>,
   pub route_len: f32,
}

/// Something which can be collected
#[derive(Default, Clone, Debug)]
pub struct Collectable {
   /// The next target
   next: Option<usize>
}

/// Manages the lifetime of collection games
pub fn collection_game(
   subs: &mut Subscriptions,
   entities: &mut EntitySet,
   game: &mut Store<CollectionGame>,
   collector: &mut Store<Collector>,
   collectable: &mut Store<Collectable>,
   transform: &mut Store<Transform>,
   mass: &mut Store<Mass>,
   velocity: &mut Store<Velocity>,
   c_mask: &mut Store<CollisionMask>,
   c_poly: &mut Store<CollisionPoly>,
   flight: &mut Store<Flight>,
   fitness: &mut Store<Fitness>,
   rb: &mut Store<RigidBody>,
) {
   let query = query!(subs, (CollectionGame), (game));

   for e in query.added() {
      // Initialize the game
      let mut next = None;
      let target_poly = esc::resources::Res::new(Poly::ellipse(10, COLLECT_DIST * 1.8 - 10.0, COLLECT_DIST * 1.8 - 10.0));
      for _ in 0..2 {
         let target = entities.create();
         collectable.insert(target, Collectable{ next });
         transform.emplace(target);
         mass.insert(target, Mass { linear: 1.0, angular: 1.0 });
         velocity.emplace(target);
         c_mask.insert(target, CollisionMask { id: 0b001, mask: 0b111 });
         c_poly.insert(target, CollisionPoly { hull: target_poly.clone() });
         rb.emplace(target);
         game[e].targets.push(target);
         next = Some(target);
      }
      game[e].targets.reverse();
   }

   for e in query.removed() {
      // Destroy the game
      for &t in &game[e].targets { entities.release(t); }
      for &o in &game[e].obstacles { entities.release(o); }
   }

   let mut worlds = query.iter().collect::<Vec<_>>();

   for e in worlds.drain(..) {
      // Game ends if time ends or any collector has no target.
      game[e].time = (game[e].time + 1) % 400;

      let end = game[e].time == 0 ||
         subs.iter::<(Collector,)>((collector,)).any(|e| collector[e].target.is_none());

      if game[e].time == 1 {
         for e in subs.iter::<(Collector, Transform)>((collector, transform)) {
            fitness[e].value = 0.0;
         }
      }

      for &t in &game[e].targets {
         velocity[t].linear *= 0.9;
         velocity[t].angular *= 0.9;
      }

      if end {
         let mut rng = thread_rng();
         game[e].time = 0;

         //let start = vec2(rng.gen_range(100.0, 1180.0), rng.gen_range(100.0, 620.0));
         let start = vec2(1280.0, 720.0) / 2.0;
         let exclude = bounds((-200.0, 200.0), (-200.0, 200.0)) + start;
         let mut prev = start;
         let mut len = 0.0;

         for &t in &game[e].targets {
            transform[t].position = vec2(
               rng.gen_range(100.0, 1180.0),
               rng.gen_range(100.0, 620.0)
            );
            //velocity[t].linear = -(transform[t].position - vec2(1280.0, 720.0) / 2.0) * rng.gen_range(0.001, 0.005);
            len += (prev - transform[t].position).length();
            prev = transform[t].position;
         }

         // Randomize obstacles
         for (e,m) in mass.iter() {
            if m.angular == 0.0 {
               transform[e].scale = vec2(rng.gen_range(2.0, 5.0), rng.gen_range(5.0, 8.0));
               transform[e].orientation = rng.gen_range(-PI, PI);
               let mut pos = start;
               while exclude.contains(pos) {
                  pos = vec2(rng.gen_range(0.0 - 200.0, 1280.0 + 200.0), rng.gen_range(0.0 - 200.0, 720.0 + 200.0));
               }
               transform[e].position = pos;
            }
         }

         let first_target = Some(game[e].targets[0]);
         let angle = rng.gen_range(-PI, PI);

         for e in subs.iter::<(Collector, Transform)>((collector, transform)) {
            transform[e].position = start;
            transform[e].orientation = angle;
            collector[e].target = first_target;
            collector[e].journey = 0.0;
            collector[e].prev_pos = Some(transform[e].position);
            collector[e].route_len = len;
            flight[e] = Flight::default();
            flight[e].energy = 100.0;
         }

         unimplemented!("EndEpoch");
      }
   }
}

/// Sets the behaviour for collector bots
pub fn collector_bots(
   subs: &mut Subscriptions,
   collector: &Store<Collector>,
   collectable: &Store<Collectable>,
   velocity: &Store<Velocity>,
   transform: &Store<Transform>,
   target: &mut Store<Target>,
) {
   let iter = subs.iter::<(Collector, Target)>((collector, target));

   for e in iter {
      if let Some(t) = collector[e].target {
         target[e].position = transform[t].position;
         target[e].velocity = velocity[t].linear;
         if let Some(t) = collectable[t].next {
            target[e].forward = (target[e].position - transform[t].position).normalize();
         } else {
            target[e].forward = Vec2::zero();
         }
      }
   }
}

/// Collectors collecting collectables.
pub fn collect_collectables(
   subs: &mut Subscriptions,
   collector: &mut Store<Collector>,
   transform: &Store<Transform>,
   collectable: &Store<Collectable>,
   fitness: &mut Store<Fitness>,
   collision: &Collision,
) {
   let iter = subs.iter::<(Collector, Transform)>((collector, transform));

   for e in iter {
      if let Some(target) = collector[e].target {
         let pos = transform[e].position;
         let target_pos = transform[target].position;
         let to_target = target_pos - pos;

         // Calculate how far towards the next collectable we've traveled.
         if let Some(prev_pos) = collector[e].prev_pos {
            fitness[e].value +=
               (pos - prev_pos).dot(to_target.normalize()) / collector[e].route_len;
         }
         collector[e].prev_pos = Some(pos);

         // Maybe collect the collectable?
         if to_target.length() < COLLECT_DIST {
            collector[e].target = collectable[target].next;
            fitness[e].value += 0.1;
         }
      }
   }

   /*
   for c in &collision.contacts {
      if collector.has(c.a) { fitness[c.a].value -= 0.001 }
      if collector.has(c.b) { fitness[c.b].value -= 0.001 }
   }
   */

}

use esc::render::*;

/// Draw the outlines of collectables
pub fn draw_collectables(
   entities: &mut Subscriptions,
   collectable: &Store<Collectable>,
   transform: &Store<Transform>,
   view: &Store<View>,
   program: &<&'static SolidColour as Material>::Program,
) {
   let views: Vec<&View> = entities.iter::<(View,)>((view,)).map(|e| &view[e]).collect();

   for e in entities.iter::<(Collectable, Transform)>((collectable, transform)) {
      let mut i = 0;
      let mut j = e;
      while let Some(next) = collectable[j].next { i += 1; j = next; }

      debug_poly(
         transform[e].to_matrix(),
         &Poly::ellipse(10, COLLECT_DIST * 1.8, COLLECT_DIST * 1.8),
         Colour::new(0.4, i as f32 * 0.2, 0.3, 1.0)
      ).draw(&views, program)
   }
}
