use derivative::*;
use serde::*;
use esc::storage::*;
use esc::geom::*;
use std::ops::IndexMut;
use super::*;

pub const MIN_SPEED: f32 = 2.45;
pub const RECOVER_SPEED: f32 = 4.24;
pub const LINEAR_MOMENTUM_SCALE: f32 = 0.953;
pub const ANGULAR_DAMPING_FACTOR: f32 = 0.8;

/// The dynamics of a plane in flight.
#[derive(Debug, Clone, Copy, Serialize, Deserialize, Derivative)]
#[derivative(Default)]
#[serde(default)]
pub struct Flight {
   /// Engine throttle, 0-1
   #[derivative(Default(value="1.0"))]
   pub throttle: f32,
   /// Kinetic energy, determines forward velocity in flight
   #[derivative(Default(value="100.0"))]
   pub energy: f32,
   /// True while the plane is stalled
   pub stalled: bool,
   /// Linear velocity from external forces.
   pub momentum: Vec2,
   /// Previous linear momentum
   pub momentum_prev: Vec2,
   /// Previous position
   pub pos_prev: Vec2,
   /// The fraction of the maximum turn rate.
   pub relative_turn: f32,
   /// The component of angular velocity from the plane's control surfaces.
   pub turn: f32,
   /// The component of angular velocity from external forces.
   pub angular_vel: f32,
   #[derivative(Default(value="1000.0"))]
   pub ammo: f32,
   pub pre_linear: Vec2,
   pub pre_angular: f32,
}

pub fn plane_pre_collide(
   subs: &mut Subscriptions,
   flight: &mut Store<Flight>,
   velocity: &Store<Velocity>,
   transform: &Store<Transform>,
) {
   let iter = subs.iter::<(Flight, Velocity, Transform)>
   ((flight, velocity, transform));

   for e in iter {
      flight[e].pre_angular = velocity[e].angular;
      flight[e].pre_linear = velocity[e].linear;
   }
}

pub fn plane_post_collide(
   subs: &mut Subscriptions,
   flight: &mut Store<Flight>,
   velocity: &mut Store<Velocity>,
   transform: &Store<Transform>,
) {
   let iter = subs.iter::<(Flight, Velocity, Transform)>
   ((flight, velocity, transform));

   for e in iter {
      let diff_angular = velocity[e].angular - flight[e].pre_angular;
      let diff_linear = velocity[e].linear - flight[e].pre_linear;
      velocity[e].angular = flight[e].pre_angular;
      velocity[e].linear = flight[e].pre_linear;
      add_momentum(&mut flight[e], &transform[e], diff_linear);
      flight[e].angular_vel += diff_angular;
   }
}

pub fn fly(
   subs: &mut Subscriptions,
   flight: &mut Store<Flight>,
   velocity: &mut Store<Velocity>,
   transform: &Store<Transform>,
   control: &Store<PlaneControl>,
   params: &Store<PlaneParams>,
   debug_lines: &mut esc::render::DebugLines,
) {
   let iter = subs.iter::<(PlaneControl, Flight, PlaneParams, Velocity, Transform)>
      ((control, flight, params, velocity, transform));

   for e in iter {
      let engine_speed = params[e].engine_speed;
      let engine_power = params[e].engine_power;
      let turn_penalty = params[e].fast_turn_penalty;
      let gravity = -0.117;
      let angle = transform[e].orientation;
      let forward = Vec2::from_angle(angle);
      let flight = flight.index_mut(e);

      // ================= Turning ======================

      let sign = if control[e].turn.abs() < 0.01 { 0.0 } else { control[e].turn.signum() };

      let turn_diff = sign * params[e].turn_inc;
      let turn_max = params[e].turn_max;
      let turn_clamp = f32::min(params[e].turn_max, control[e].turn.abs());

      if turn_diff.abs() < 0.005 || flight.turn.signum() != turn_diff.signum() {
         flight.turn = 0.0;
      }
      flight.turn += turn_diff;
      flight.turn = clamp_abs(flight.turn, turn_clamp);
      flight.relative_turn = flight.turn.abs() / turn_max;
      velocity[e].angular = flight.turn + flight.angular_vel;

      flight.angular_vel *= ANGULAR_DAMPING_FACTOR;
      if flight.angular_vel < 0.1 { flight.angular_vel = 0.0; }


      // ================= Engine ======================

      let throttle_diff = clamp(control[e].throttle_diff, -1.0, 1.0) * params[e].throttle_rate;
      flight.throttle += throttle_diff;
      flight.throttle = clamp(flight.throttle, 0.0, 1.0);

      if flight.stalled {
         flight.throttle = 1.0;
         velocity[e].linear = flight.momentum;
      } else {
         let speed = flight.energy.sqrt() * flight.throttle;
         let relative_speed = speed / engine_speed;

         // Drift
         if flight.relative_turn > 0.5 && relative_speed > 0.65 {
            let turn_mult = (flight.relative_turn - 0.5) * 2.0;
            let speed_mult = -(13.0 / 27.0) + f32::min(2.0, relative_speed) * (20.0 / 27.0);
            let c = 0.55 * turn_mult * speed_mult;

            let t = ((velocity[e].linear - flight.momentum_prev) - forward * speed) * c;
            add_momentum(flight, &transform[e], t);
         }

         // Combine the forward velocity from the engine with velocity from other sources.
         velocity[e].linear = forward * speed + flight.momentum;
      }
      flight.momentum_prev = flight.momentum;


      // ================= Stalling ======================

      let throttle = flight.throttle;
      //let forward_speed = velocity[e].linear.x * angle.cos() + velocity[e].linear.y * angle.sin();
      let forward_speed = velocity[e].linear.dot(forward);

      if !flight.stalled && forward_speed < MIN_SPEED {
         flight.stalled = true;
      } else if flight.stalled && forward_speed * throttle > RECOVER_SPEED {
         flight.stalled = false;
         flight.energy = forward_speed * forward_speed;
         flight.momentum -= forward * forward_speed * throttle;
      }


      // ================= Kinetic Energy ======================

      // Loose energy for hard turns
      if flight.relative_turn > 0.5 { flight.energy *= turn_penalty; }

      // Gravitational energy
      flight.energy += -2.0 * gravity * -flight.momentum.y as f32;

      // Minimum energy
      flight.energy = f32::max(flight.energy, 0.1);

      let engine_speed_sq = engine_speed * engine_speed;

      fn drag(speed_sq: f32, threshold_sq: f32) -> f32 {
         let drag = (1.0 - (0.0019 * (speed_sq as f64 / threshold_sq as f64).powf(2.5))) as f32;
         assert!(drag <= 1.0);
         drag
      }

      if flight.stalled {
         flight.momentum.y += gravity;

         // Apply drag to fast-moving stalled planes (e.g. when using reverse thrust)
         if flight.momentum.length_sq() > engine_speed_sq {
            let speed_sq = flight.momentum.length_sq();
            flight.momentum *= drag(speed_sq, engine_speed_sq);
         }
      } else {
         if flight.momentum.length_sq() < 0.01 {
            flight.momentum = vec2(0.0, 0.0);
         } else {
            flight.momentum *= LINEAR_MOMENTUM_SCALE;
         }

         if flight.energy < engine_speed_sq && forward_speed < engine_speed {
            flight.energy += engine_power;
         } else if flight.energy > 1.005 * engine_speed_sq || forward_speed > engine_speed {
            let drag = drag(flight.energy, engine_speed_sq);

            flight.energy = f32::max(flight.energy * drag * drag, 0.1);
         }
      }

      flight.pos_prev = transform[e].position;

      // Afterburner
      if throttle_diff > 0.0 && flight.throttle > 0.999 {
         let power = f32::min(flight.ammo, params[e].afterburner_power);
         let mut f = 0.3 * power / params[e].afterburner_power; // TODO: load factor

         if params[e].easy_stall_recovery {
            f *= map(forward_speed, [4.24, 2.756 + 0.35 * engine_speed], [2.0, 1.0]);
         }
         
         let pos = transform[e].position;
         let behind = Vec2::from_angle(std::f32::consts::PI + transform[e].orientation);
         debug_lines.lines.push((pos, pos + behind * 50.0));

         println!("{} {} {}", flight.ammo, power, f);
         add_forward_speed(flight, &transform[e], f);
         flight.ammo -= power;
      }

      // Reverse thrust
      if params[e].reverse_thrust && throttle_diff < 0.0 && (flight.stalled || flight.throttle < 0.1) {
         flight.ammo -= f32::min(flight.ammo, 0.8333 * params[e].ammo_regen);
         let fraction = map(forward_speed, [-engine_speed, 0.1 * -engine_speed], [0.0, 2.9]);

         let f = -0.44 * fraction; // TODO: load factor
         add_forward_speed(flight, &transform[e], f);
      }

      // Ammo regen
      flight.ammo = clamp(flight.ammo + params[e].ammo_regen, 0.0, params[e].max_ammo);
   }
}

/// Add some linear velocity to a plane
pub fn add_momentum(flight: &mut Flight, transform: &Transform, mut force: Vec2) {
   if !flight.stalled {
      // Redirect the forward component of the force to modifying engine energy.
      let angle = transform.orientation;
      let diff = force.x * angle.cos() + force.y * angle.sin();
      let new_speed = flight.energy.sqrt() + diff;
      if new_speed > 0.0 {
         force -= Vec2::from_angle(angle) * diff;
         flight.energy = new_speed * new_speed;
      } else {
         force += Vec2::from_angle(angle) * flight.energy.sqrt();
         flight.energy = 0.0;
      }
   }

   flight.momentum += force;
}

/// Add some speed in the direction we're facing
pub fn add_forward_speed(flight: &mut Flight, transform: &Transform, force: f32) {
   let angle = transform.orientation;
   let force = vec2(force * angle.cos(), force * angle.sin());
   add_momentum(flight, transform, force);
}
