use serde::*;
use esc::input::*;
use esc::storage::*;
use esc::util::time::*;
use esc::geom::*;
use esc::render::View;

/// Plane input
#[derive(Copy, Clone, Default, Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct PlaneControl {
   /// Relative throttle difference (-1 to 1)
   pub throttle_diff: f32,
   /// Amount to turn in degrees
   pub turn: f32,
   pub use_item: bool,
}

pub fn player_plane_controls(
   time: &GameTime,
   subs: &mut Subscriptions,
   input: &Store<Controller>, 
   plane: &mut Store<PlaneControl>,
   transform: &Store<Transform>,
   view: &Store<View>,
) {
   let tick = time.tick;

   let cursor_transform = view.iter().next()
      .map(|(_, view)| view.screen_to_world())
      .unwrap_or(Mat3::id());

   for e in subs.iter::<(Controller, PlaneControl, Transform)>((input, plane, transform)) {
      let mut throttle = 0.0;

      if input[e].is_pressed("up", tick) { throttle += 1.0 }
      if input[e].is_pressed("down", tick) { throttle -= 1.0 }

      let cursor = cursor_transform * vec2(input[e].axis("x"), input[e].axis("y"));
      let offset = cursor - transform[e].position;
      let turn = angle_diff(offset.angle(), transform[e].orientation);

      plane[e].throttle_diff = throttle;
      plane[e].turn = turn;
   }
}
