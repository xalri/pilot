use esc::geom::*;
use esc::resources::*;
use esc::storage::*;
use esc::error::*;
use esc::render::*;
use std::f32::consts::PI;
use super::*;
use serde::*;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct PlaneAnim {
   pub frames: Vec<(f32, Res<Poly>, Res<Vertices<Vertex2>>)>,
   pub bank_angle: f32,
   #[serde(default)]
   pub spin: f32,
   #[serde(default)]
   pub spin_rate: f32,
   #[serde(default)]
   pub flipped: bool,
   #[serde(default)]
   pub flip_in_progress: bool,
}

pub fn flip_rate(bank_angle: f32) -> f32 {
   let rate_of_role = 16.0;
   let degrees_to_roll = 180.0 - 2.0 * bank_angle;
   let updates_to_roll = (degrees_to_roll / rate_of_role).ceil();
   degrees_to_roll / updates_to_roll - 0.1
}

pub fn update_plane_anim(
   subs: &mut Subscriptions,
   anim: &mut Store<PlaneAnim>,
   flight: &Store<Flight>,
   transform: &Store<Transform>,
   collision_poly: &mut Store<CollisionPoly>,
   mesh: &mut Store<Mesh<Vertex2>>,
) {
   let iter = subs.iter::<(PlaneAnim, Flight, Transform)>((anim, flight, transform));

   for e in iter {
      let mut anim = &mut anim[e];
      let flight = &flight[e];
      let o = transform[e].orientation;

      let flip_rate = flip_rate(anim.bank_angle).to_radians();
      let bank_angle = anim.bank_angle.to_radians();
      let max_bank_rate = bank_angle / 6.0;

      let threshold_left: f32 = f32::to_radians(90.0 + 60.0);
      let threshold_right: f32 = f32::to_radians(90.0 - 60.0);

      let flip_left = o > threshold_left || o < -threshold_left;
      let flip_right = o < threshold_right && o > -threshold_right;

      let prevent_flip = flight.stalled || flight.relative_turn > 0.97;

      if !anim.flip_in_progress && !prevent_flip && ((!anim.flipped && flip_left) || (anim.flipped && flip_right)) {
         anim.flip_in_progress = true;
         anim.spin_rate = if flip_left ^ (anim.spin < 0.0) { flip_rate } else { -flip_rate };
      }

      if anim.flip_in_progress {
         let final_angle = bank_angle;
         let new_spin = wrap_angle(anim.spin + anim.spin_rate);
         if anim.flipped {
            if -final_angle < new_spin && new_spin < final_angle {
               anim.flipped = false;
               anim.flip_in_progress = false;
               anim.spin_rate = 0.0;
            }
         } else {
            if new_spin > PI - final_angle || new_spin < PI - final_angle {
               anim.flipped = true;
               anim.flip_in_progress = false;
               anim.spin_rate = 0.0;
            }
         }
      }

      if !anim.flip_in_progress {
         let relative_turn = flight.relative_turn * flight.turn.signum();// + flight.body.t.angular_vel * 0.5;
         let mut so = anim.spin;
         if anim.flipped { so = if so < 0.0 { so + PI } else { so - PI } }
         let target = bank_angle * relative_turn;
         anim.spin_rate = clamp(target - so, -max_bank_rate, max_bank_rate);
      }

      anim.spin = wrap_angle(anim.spin + anim.spin_rate);

      let mut diff = 2.0 * PI;
      let mut frame = 0;

      for i in 0..anim.frames.len() {
         let d = wrap_angle(anim.frames[i].0.to_radians() - anim.spin).abs();
         if d < diff {
            frame = i;
            diff = d;
         }
      }

      collision_poly[e].hull = anim.frames[frame].1.clone();
      mesh[e].vertices = anim.frames[frame].2.clone();
   }
}
