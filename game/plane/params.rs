use esc::storage::*;
use serde::*;

/// A plane's configuration.
#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct PlaneParams {
   /// The plane's base speed
   pub base_speed: f32,
   /// The power of the engine, higher values -> more acceleration
   pub engine_power: f32,
   /// Turning acceleration
   pub turn_speed: f32,
   /// The maximum turn speed
   pub max_turn: f32,
   /// The energy multiplier while in a tight turn.
   pub fast_turn_penalty: f32,
   /// The fraction of emp's turning reduction to resist
   pub emp_resistance: f32,
   /// The plane's weight
   pub weight: f32,
   /// The rate at which the plane's throttle can change
   pub base_throttle_rate: f32,
   /// Allows the plane to accelerate in reverse
   pub reverse_thrust: bool,
   /// Gives a boost to the afterburner inversely proportional to the plane's forward
   /// speed to help recover from stalls.
   pub easy_stall_recovery: bool,
   /// The power of the afterburner
   pub afterburner_power: f32,
   /// The plane's maximum health
   pub max_health: f32,
   /// Ammo regeneration per tick
   pub base_ammo_regen: f32,
   /// The plane's ammo capacity
   pub max_ammo: f32,
   /// The fraction of the plane's speed achieved while carrying a bomb
   pub bomb_speed: f32,
   /// The fraction of the plane's speed achieved while carrying a ball
   pub ball_speed: f32,
   /// The base speed of a fired ball
   pub ball_shoot_speed: f32,
   /// The fraction of the afterburner's power available while carrying a bomb.
   pub bomb_afterburner_power: f32,
   /// Veteran bars give a greater boost
   pub ace_instincts: bool,
   /// The degree to which the plane rolls (used in animation)
   pub bank_angle: f32,

   // Derived parameters
   /// The rate of ammo regeneration after veteran modifiers
   #[serde(skip)]
   pub ammo_regen: f32,
   /// The engine speed after veteran modifiers
   #[serde(skip)]
   pub engine_speed: f32,
   /// The max rate of change of the throttle after veteran modifiers
   #[serde(skip)]
   pub throttle_rate: f32,
   /// The change in angular velocity per tick.
   #[serde(skip)]
   pub turn_inc: f32,
   /// The maximum angular velocity from the plane's control surfaces.
   #[serde(skip)]
   pub turn_max: f32,
   /// A damage multiplier.
   #[serde(skip)]
   pub damage_multi: f32,
}

#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Experience {
   pub value: i32,
}

#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Emp {
   // TODO: timer
}

impl PlaneParams {
   /// Add some amount of emp resistance (non-linearly)
   pub fn compound_emp_resist(&mut self, b: f32) {
      self.emp_resistance += (1.0 - self.emp_resistance) * b;
   }
}

/// Calculate plane parameters from base parameters & experience
pub fn derive_plane_params(
   subs: &mut Subscriptions,
   params: &mut Store<PlaneParams>,
   exp: &Store<Experience>,
   emp: &Store<Emp>,
) {

   for e in subs.iter::<(PlaneParams, Experience)>((params, exp)) {
      let mut params = &mut params[e];
      let vet_bars = (exp[e].value / if params.ace_instincts { 8 } else { 10 }).min(9);

      params.ammo_regen = {
         let per_bar = if params.ace_instincts { 1.019 } else { 1.013 };
         params.base_ammo_regen * f32::powi(per_bar, vet_bars)
      };

      params.engine_speed = {
         let mut per_bar = params.base_speed * 0.01;
         if params.ace_instincts { per_bar *= 1.4; }
         params.base_speed + vet_bars as f32 * per_bar
      };

      params.throttle_rate = {
         params.base_throttle_rate * if emp.has(e) { 0.5 } else { 1.0 }
      };

      params.damage_multi = {
         let per_bar = if params.ace_instincts { 0.03 } else { 0.02 };
         1.0 + vet_bars as f32 * per_bar
      };

      {
         let mut inc = params.turn_speed;
         let mut max = params.max_turn;

         let per_bar = if params.ace_instincts { 1.014717 } else { 1.01 };

         inc *= f32::powi(per_bar, vet_bars);
         max *= f32::powi(per_bar, vet_bars);

         if emp.has(e) && max > 3.385 {
            let mut x = (3.385 + (max - 3.385) * 0.33) / max;
            x = params.emp_resistance + ((1.0 - params.emp_resistance) * x);

            inc *= x;
            max *= x;
         }

         params.turn_inc = inc;
         params.turn_max = max;
      }
   }
}
