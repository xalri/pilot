use super::*;

/// A Plane setup
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PlaneSetup {
   pub plane: PlaneKind,
   pub green_perk: GreenPerk,
   pub blue_perk: BluePerk,
}

/// The type of a plane and primary perk.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PlaneKind {
   Biplane(BiplanePerk),
   Bomber(BomberPerk),
   Explodet(ExplodetPerk),
   Loopy(LoopyPerk),
   Miranda(MirandaPerk),
}

/// A Loopy's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum LoopyPerk { Tracker, DoubleFire, Acid }

/// A Bomber's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BomberPerk { Suppressor, Bombs, Flak }

/// An Explodet's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum ExplodetPerk { Director, Thermo, Remote }

/// A Biplane's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BiplanePerk { Dogfighter, Recoilless, HeavyCannon }

/// A Miranda's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum MirandaPerk { Trickster, Laser, TimeAnchor }

/// A green perk, modifies the plane parameters.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum GreenPerk { None, Rubber, Heavy, Repair, Flexi }

/// A blue perk, modifies the plane parameters.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BluePerk { None, Turbo, Ultra, Reverse, Ace }

impl GreenPerk {
   /// Apply the perk to a set of plane parameters
   pub fn update_params(&self, params: &mut PlaneParams) {
      match *self {
         GreenPerk::Rubber => {
            params.compound_emp_resist(0.2)
         },
         GreenPerk::Heavy => {
            params.max_health = 17.0 + params.max_health * 1.16
         },
         GreenPerk::Flexi => {
            params.fast_turn_penalty = 0.995;
            params.turn_speed *= 1.2;
            params.max_turn *= 1.2;
         }
         GreenPerk::Repair => (),
         GreenPerk::None => (),
      }
   }
}

impl BluePerk {
   /// Apply the perk to a set of plane parameters
   pub fn update_params(&self, params: &mut PlaneParams) {
      match *self {
         BluePerk::Ultra => params.max_ammo = 1250.0,
         BluePerk::Turbo => params.base_ammo_regen *= 1.2,
         BluePerk::Ace => params.ace_instincts = true,
         BluePerk::Reverse => {
            params.reverse_thrust = true;
            params.base_throttle_rate *= 1.9;
         }
         BluePerk::None => (),
      }
   }
}

