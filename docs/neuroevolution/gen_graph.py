import matplotlib.pyplot as plt
import pandas as pd
import glob
import operator

def do_thing(name):
    files = glob.glob(name)
    if len(files) == 0:
        return None
    df = pd.read_csv(files[0])
    df.set_index('elapsed', inplace=True)
    del df['fitness']
    del df['generation']
    del df['step_size']
    lines = []
    _name = name[6:-6]
    for filename in files:
        name = str(filename[6:-4].replace('_',' '))
        tmp = pd.read_csv(filename)
        tmp.set_index('elapsed', inplace=True)
        del tmp['generation']
        del tmp['step_size']
        window = int(len(tmp) / 20)
        #tmp['fitness'] = tmp['fitness'].rolling(min_periods=1, window=window).mean()
        tmp.rename(index=str, columns={"fitness": name}, inplace=True)
        df = df.join(tmp, how='outer')
        lines += [name]
        df.index = df.index.map(int)
    df.fillna(method='backfill', inplace=True)
    df.fillna(method='pad', inplace=True)
    df.sort_index(inplace=True)
    df = df.groupby(df.index).first()
    df = df.reindex(sorted(df.columns), axis=1)
    
    for l in lines:
        df[l] = df[l].rolling(min_periods=1, window=30).mean()
    
    return (_name, df, lines)

res = [do_thing(x) for x in [
  #'./csv/single_layer_*.csv',
  #'./csv/multi_layer_*.csv',
  #'./csv/obstacles_*.csv',
  #'./csv/population_*.csv',
  #'./csv/activation_*.csv',
  demo.csv
]]

res = [x for x in res if not x is None]

plots = []

for (name, df, lines) in res:
    plot = df.plot(y=[*lines])
    handles, labels = plot.get_legend_handles_labels()
    hl = sorted(zip(handles, labels), key=operator.itemgetter(1))
    a, b = zip(*hl)
    plot.legend(a,b)
    plot.set_ylim(bottom=-1, top=1)
    plot.get_figure().savefig(f'docs/images/{name}.png', dpi=200)
    
