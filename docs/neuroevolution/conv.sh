#!/bin/bash
rm csv/*
sed -i.bak ':begin;$!N;s/,\n]/\n]/g;tbegin;P;D' data/*.log
for file in data/*.log
do 
   jq -r '.data | (.[0] | keys_unsorted) as $keys | $keys, map([.[ $keys[] ]])[] | @csv' $file \
      > "${file%.log}.csv"
done
mv data/*.csv csv/
find csv/ -size 0 -print0 | xargs -0 rm --
