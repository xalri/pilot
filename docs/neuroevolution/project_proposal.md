---
title: "CSC8490 Interim Report"
subtitle: "Machine Learning for Real-Time Game AI"
author: "Lewis Hallam"
bibliography: references.bib
output: pdf_document
header-includes:
 - \usepackage{tikz}
 - \linespread{1.03}
...

### Introduction

Real-time competitive games are an interesting subject for machine learning 
research, partly due to their large input space, complex behaviours, and limited 
time for computing a response.

From the perspective of a game developer these same factors make implementing AI
agents more difficult.
It's desirable for competitive games to have artificial players to play both
against and alongside real players. The artificial players should ideally have 
the same capabilities as the real players, and act in a way which is enjoyable
to play against. Traditional techniques use state machines and decision trees
for high-level decision making and specific algorithms for movement. 
These agents are difficult to implement and are rarely capable of keeping up
with experienced players.
Recently machine learning has been considered as a possible tool for creating 
agents which behave more like human players, with potentially lower development
time.

This project aims to apply current techniques in machine learning to a 
competitive commercial game and compare the results to it's existing AI.

#### Context

The specific subject of the project is Altitude, a multiplayer aerial combat game
released in 2009.
Altitude uses state-machine based artificial players. Their ability to navigate 
complex maps is limited and they can't keep up with even moderately experienced 
players.


### Aim & Objectives

The aim of the project is to create an ML agent for playing Altitude.
The objectives are to:

 - Create scenarios of various difficulty in which to train and test agents.
 - Implement a state-machine based agent similar to that in Altitude as a baseline.
 - Implement an ML based agent.
 - Compare the effectiveness of different topologies and input for the ML agent.
 - Record the relative performance of the final ML agent and the baseline agent.

