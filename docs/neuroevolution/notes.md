
# Motivation
 
### Neuroevolution

https://github.com/Alro10/awesome-deep-neuroevolution
"Playing atari with six neurons" etc

 - given current & target angle, neural network kinda learns to do a - b
     - .. but wrapping angles are hard, always gets stuck at 0 degrees.
 - Given unit vectors for current & target orientation does not work,
   with either a relative target vector or a turn angle as the output.
 - Everything lives for 3 generations? maybe helps? why?
 - My optimisation process for the hyperparameters is probably not much better than neuroevolution ._.
 
 - Going to a single point is not a good training environment since fitness on a single test is
   not indicitive of fitness in the average case.
 - A good fitness measure may be particularly important for ES (Evolution Strategies), since the best
   agent after every step becomes the new baseline from which new agents are derived, similar to
   gradient descent.
  
 - So.. upgrade the test to following to a sequence of points, agents are less likely to be lucky.

http://blog.otoro.net/2017/11/12/evolving-stable-strategies/

 > We don’t want our natural selection process to allow agents with weak policies who had gotten lucky with an easy map to advance to the next generation. We also want to give agents with good policies a chance to redeem themselves.


Making things work for vectors:
 - wasn't learning anything, so went back to angle based
 - fixed things to work with angles
 - made the network more complex, continued to fix things to work with angles
 - main thing was massively increasing exploration
 - switched back to vectors, same topology now works.
 - now 2 hidden layers, 20 nodes each, relu activation
 - ^^^^ relu activation is probably important
 - removed normalization of velocity -> still works though takes longer to train
 - removed normalization of relative position
    - much slower to train, seems to be less stable

 - Doesn't work with no activation function in hidden layers
 - Solution in less than 1000 generations:
    - input is normalised relative target position & normalised velocity
    - output is x and y of a direction vector, converted to a turn angle.
    - each generation pertubes parameters with sd = 10
    - one hidden layer, 10 nodes, relu activation
    - output layer is 2 nodes, tanh activation
    
Adding obstacles:
 - raycast input
 - at this point normalization is a problem, since single fixed s.d. for all parameters
 - enter cma-es
    
# CMA-ES

 cma-es tutorial: https://arxiv.org/pdf/1604.00772.pdf
 on the value of ES for high dimensional reinforcement learning: https://arxiv.org/pdf/1806.01224.pdf
https://en.wikipedia.org/wiki/CMA-ES

 - Adapts to badly scaled objective functions 
  -> for neural nets, obviates normalization of input? test this
 - Doesn't require a large population size (TODO: numbers)
 - Prevents premature convergence
 - Somewhat difficult to implement (eigendecomposition etc)
 
 - wtf
 - minimal implementation, without rank-1 update & variable step size etc:
    - working solution in like 10 generations.
 
 
# Topology

 - Training dynamically? EANT2
 https://www.informatik.uni-kiel.de/inf/Sommer/doc/Publications/nts/SiebelSommer-IJHIS2007.pdf
 
 ^^^^ Many good references

# Training

 - curse of dimensionality

\begin{thebibliography}{25}
<div id="refs>

<\div>
\end{thebibliography}
