#![feature(fn_traits)]

use esc::resources::*;
use esc::geom::*;
use esc::render::*;
use esc::window::*;
use esc::storage::*;

use game::world::*;
use game::scenarios::collect::*;
use game::bot::*;
use agent::optimization::*;
use agent::math::*;
use agent::model::*;

use std::time::*;
use std::fs::File;
use std::io::*;
use std::sync::Arc;
use std::thread;
use antidote::Mutex;
use serde::*;
use rayon::prelude::*;

mod common;
use self::common::*;

fn main() {
   simple_logger::init_with_level(log::Level::Debug).unwrap();
   let mut window = Window::new("pilot", vec2i(1280, 720));
   res::push_source(Directory::open("res"));
   let mut s = Storage::default();

   let path = std::env::args().skip(1).next().unwrap();
   let log = format!("{}.log", &path);
   let nn = format!("{}.nn", &path);

   let mut log_file = BufReader::new(File::open(&log).unwrap());

   let line = log_file.lines().skip(1).next().unwrap().unwrap();
   let line = &line[6..line.len() - 1];
   let run: Run = serde_json::from_str(line).unwrap();
   println!("{:?}", run);

   create_world(&mut s, &run);

   let view = s.entities.create();
   s.view.insert(view, View::fill(1280, 720));

   pub struct EvQueue;
   impl UniqueEvQueue for EvQueue { type Ev = EndEpoch; }

   loop {
      window.poll_events();
      if window.close_requested { break }
      tick(&mut s, true);

      while let Some(_) = s.events.get::<EvQueue>() {
         // Get the latest parameter set
         if let Ok(mut file) = std::fs::File::open(&nn) {
            let mut rng = rand::thread_rng();
            use rand::distributions::Distribution;

            let cma: CMA = bincode::deserialize_from(&mut file).unwrap();
            let dist = MultivariateNormal::new(cma.mean, cma.covar);
            for a in &mut s.agent_navigator {
               a.network.params = dist.sample(&mut rng);
            }
            println!("Training time: {:.1}s", agent::time::ns_to_seconds(cma.training_time));
            println!("Generation:    {}", cma.generation);
            println!("Fitness:       {:.3}", cma.fitness);
            println!("Step size:     {:.2}", cma.step_size);
         }
      }

      let mut frame = window.draw();
      s.view[view].start_frame(&mut frame);
      draw(&mut s);
      s.view[view].end_frame();
      frame.finish().unwrap();
      std::thread::sleep(Duration::from_millis(16));
   }
}
