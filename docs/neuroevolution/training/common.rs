use esc::resources::*;
use esc::geom::*;
use esc::render::*;
use esc::window::*;
use esc::storage::*;

use game::world::*;
use game::scenarios::collect::*;
use game::bot::*;
use agent::optimization::*;
use agent::math::*;
use agent::model::*;

use std::time::*;
use std::fs::File;
use std::io::*;
use std::sync::Arc;
use std::thread;
use antidote::Mutex;
use serde::*;
use rayon::prelude::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Run {
   pub population: usize,
   pub obstacles: usize,
   pub network: NetworkDescriptor,
   pub log: String,
   pub nn: String,
   pub initial_sd: f32,
   pub limit: u64,
}

pub fn create_world(s: &mut Storage, params: &Run) {
   tick(s, false); // Register event queues

   // Create the collection scenario
   let game = s.entities.create();
   s.collection_game.emplace(game);

   // Create some planes to train
   let proto = res::get::<Entity>("loopy").unwrap();
   let mut plane = 0;
   for _ in 0..params.population {
      plane = proto.reify(s);
      s.transform[plane].position = vec2(100.0, 300.0);
      s.target.emplace(plane);
      s.collector.emplace(plane);
      s.fitness.emplace(plane);

      let network = params.network.clone().build();
      let navigator = NeuralNavigator { network, fitness: 0.0, memory: vec![] };
      s.agent_navigator.insert(plane, navigator);
   }

   let obstacle = res::get::<Entity>("obstacle").unwrap();
   use std::f32::consts::PI;
   let mut rng = thread_rng();
   for _ in 0..params.obstacles {
      let e = obstacle.reify(s);
      s.transform[e].scale = vec2(rng.gen_range(2.0, 5.0), rng.gen_range(5.0, 8.0));
      s.transform[e].orientation = rng.gen_range(-PI, PI);
      s.transform[e].position = vec2(rng.gen_range(0.0 - 200.0, 1280.0 + 200.0), rng.gen_range(0.0 - 200.0, 720.0 + 200.0));
   }

   // Generate an initial population
   use rand::prelude::*;
   let mut mean = s.agent_navigator[plane].network.params.clone();
   let mut dist = rand::distributions::Normal::new(0.0, params.initial_sd as f64);

   if let Ok(mut file) = std::fs::File::open(&params.nn) {
      mean = bincode::deserialize_from(&mut file).unwrap();
      let dist = rand::distributions::Normal::new(0.0, 0.0);
   }

   let mut rng = thread_rng();
   for a in &mut s.agent_navigator {
      a.network.params.clone_from(&mean);
      a.network.params.apply(|v| v + dist.sample(&mut rng) as f32);
   }
}
