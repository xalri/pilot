#![feature(fn_traits)]

use esc::resources::*;
use esc::geom::*;
use esc::render::*;
use esc::window::*;
use esc::storage::*;

use game::world::*;
use game::scenarios::collect::*;
use game::bot::*;
use agent::optimization::*;
use agent::math::*;
use agent::model::*;

use std::time::*;
use std::fs::File;
use std::io::*;
use std::sync::Arc;
use std::thread;
use antidote::Mutex;
use serde::*;
use rayon::prelude::*;

mod common;
use self::common::*;

fn train(params: Run) {
   let start_time = Instant::now();
   let mut log = File::create(&params.log).unwrap();

   let mut s = Storage::default();
   create_world(&mut s, &params);

   // Set the training strategy
   let mut generation = 0;
   let n = s.agent_navigator.iter().next().unwrap().1.network.params.len(); // number of parameters
   let mut strategy = CMA::new(n);
   strategy.step_size = params.initial_sd;

   pub struct EvQueue;
   impl UniqueEvQueue for EvQueue { type Ev = EndEpoch; }

   writeln!(log, "{{");
   writeln!(log, "\"run\": {},", serde_json::to_string(&params).unwrap());
   writeln!(log, "\"data\": [");
   loop {
      tick(&mut s, params.obstacles > 0);

      while let Some(_) = s.events.get::<EvQueue>() {
         generation += 1;

         let mut individuals = vec![];
         let mut idx = vec![];
         let mut fitness = 0.0;
         let mut n = 0;
         for i in s.subscriptions.iter::<(Fitness, NeuralNavigator)>((&mut s.fitness, &mut s.agent_navigator)) {
            idx.push(i);
            individuals.push(Individual{
               parameters: s.agent_navigator[i].network.params.clone(),
               fitness: s.fitness[i].value,
            });
            if s.fitness[i].value.is_finite() { fitness += s.fitness[i].value; }
            n += 1;
         }
         fitness /= n as f32;
         strategy.solve(&mut individuals);
         let best_idx = best(&individuals);

         for (idx, entity) in idx.drain(..).enumerate() {
            s.agent_navigator[entity].network.params = individuals[idx].parameters.clone();
            s.agent_navigator[entity].memory.clear();
         }

         writeln!(log, "{{ \"generation\": {}, \"elapsed\": {}, \"fitness\": {}, \"step_size\": {} }},",
                  generation,
                  start_time.elapsed().as_secs(),
                  fitness,
                  strategy.step_size);
         let tmp = format!("{}.tmp", params.nn);
         let tmp = std::path::Path::new(&tmp);
         let mut w = std::fs::File::create(&tmp).unwrap();
         bincode::serialize_into(&mut w, &strategy).unwrap();
         std::fs::rename(tmp, &params.nn);
      }

      if start_time.elapsed() > Duration::from_secs(params.limit) { break; }
   }
   writeln!(log, "]}}");
}

fn main() {
   simple_logger::init_with_level(log::Level::Debug).unwrap();
   res::push_source(Directory::open("res"));

   let inputs = 17;
   let outputs = 3;

   let default = Run {
      population: 50,
      obstacles: 15,
      network: NetworkDescriptor {
         inputs,
         layers: vec![
            LayerDescriptor::Linear { nodes: 20 }, LayerDescriptor::Activation(Activation::Relu),
            LayerDescriptor::Linear { nodes: 20 }, LayerDescriptor::Activation(Activation::Relu),
            LayerDescriptor::Linear { nodes: outputs }, LayerDescriptor::Activation(Activation::Tanh),
         ],
         params: None,
      },
      log: "demo.log".to_string(),
      nn: "demo.nn".to_string(),
      initial_sd: 3.0,
      limit: 60000,
   };

   train(default);
   //tests.par_iter().for_each(|test| { train(test.clone()); });
}
