#!/usr/bin/env bash
pandoc \
   --highlight-style=espresso \
   --filter=pandoc-citeproc \
   --csl=entcs.csl \
   --biblio=references.bib \
   --template=template.tex \
   -o $1.pdf \
   $1.md
pkill -HUP mupdf
