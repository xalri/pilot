---
title: "Neuroevolution for Low-Level AI in Real-Time Games"
author: "Lewis Hallam"
bibliography: references.bib
output: pdf_document
link-citations: true
header-includes:
 - \usepackage{tikz}
 
abstract: |
    We used neuroevolution to create agents which replace the low-level movement 
    and navigation algorithms of bots in Altitude, a competitive aerial combat game.
    The agents take over the task of flying the plane efficiently and avoiding obstacles,
    leaving higher level tasks like choosing the target to the existing algorithms.
    
    The agents are based on feed-forward neural networks with a pre-defined topology.
    A high-performance model of the game was built in which to test the agents,
    and custom implementations of various stochastic optimization algorithms were 
    used to train them.
    The agents can be trained quickly when placed in a simple environment but fail to 
    reliably avoid obstacles. When the pre-trained agents are injected back into the 
    game they are observerd to fly better than the game's original bots only in 
    open spaces.
    Of the tested stochastic optimization methods the covariant matrix adaptation 
    evolution strategy finds a suitable solution in significantly fewer iterations than 
    the others.
    
keywords:
 - Game AI, Neuroevolution, Neural networks, Evolutionary strategies, CMA-ES
...


# Introduction

Real-time competitive games are an interesting subject for machine learning 
research due to their large input space, complex behaviours, and limited 
time for computing a response.

From the perspective of a game developer these same factors are obstacles
to implementing artificial intelligence.
It's desirable for competitive games to have artificial players to play both
against and alongside real players. The artificial players should ideally have 
the same capabilities as the real players, and act in a way which is enjoyable
to play against. Traditional techniques use state machines and decision trees
for high-level decision making and specific algorithms for movement. 
When movement mechanics are such that there's no obvious algorithm for optimal 
movement, bot movement can appear clunky and unnatural [@AIChallenges]. Machine learning could 
both save programmer time and increase robustness as minimal changes are needed
to the agents when the game mechanics are changed, they can just be re-trained 
in the new environment.

Neuroevolution [@Neuroevolution01],[@Igel04] is a reinforcement learning technique inspired by the ideas 
of evolution. It combines neural networks and evolutionary algorithms to iteratively improve 
an approximation of an arbitrary function. Neuroevolution has been used successfully to learn to play games and control
NPCs in games [@RisiT14]. 

The specific subject of this project is Altitude [@Altitude], a team-based aerial 
combat game released in 2009. 
Altitude has bots which use a system of multiple connected algorithms each with 
specific tasks like deciding objectives, navigation, and controlling weapons and 
abilities. The bots loose to most human players with even a few hours of experience,
so don't make the best teammates or opponents.
Having multiple levels of AI communicating with each-other through simple 
interfaces gives an obvious path for incremental improvement by replacing parts 
of the system with improved versions. 

In this project an agent was built to replace the low-level flight AI in Altitude.
On each frame it takes the state of the plane, a target, and a representation of the environment, 
and outputs a change in throttle and turn to efficiently move the player towards the 
target without crashing or stalling. The environment is a function from a direction to 
the distance from the plane to the closest obstacle in that direction which the agent 
can sample to find obstacles.
The current implementation of this layer is scripted and encodes minimal knowledge 
of the dynamics of the plane -- it doesn't account for gravity, turn rate, drift, 
the dynamics of the afterburner, and so on, making it a rather dreadful pilot.

The task is well suited to neuroevolution: The performance is easily measurable;
agents can be evaluated quickly; and the inputs and outputs can be encoded as floats.
There are strong similarities to creating a controller for a racing game, to which
neuroevolution has been successfully applied in the past [@Neuroevolution02].

We implemented neuroevolution with feed-forward neural networks as the agents and the
covariance matrix adaptation evolution strategy (CMA-ES) for training. Agents were first
trained in custom scenarios inside a minimal model of the game, then transferred to
the game itself. 

## Artificial Neural networks

An artificial neural network is a collection of neurons. Each neuron has several inputs
which arrive from other neurons. The connections have an associated strength, $w_{j,i}$.
The neuron outputs the weighted sum of the inputs with an activation function $f$ applied.

$$x_j = f(\sum_{i}{w_{j,i} x_i})$$

The activation function $f$ is commonly either a sigmoid function (an "S" shaped curve, like
$\tanh(x)$ or the sigmoid function ${\e^x}\over{\e^x + 1}$) or a rectification function (which
takes only the positive part of the argument, $max(0, x)$).

Neural networks are often organized into layers, where connections only exist from one
layer to the next. These networks are called Multi Layer Perceptrons (MLPs) [@mlp].
An MLP is evaluated by passing inputs to the first layer, then calculating the values
of each neuron layer-by-layer, the final layer give the network's output.
A neural network is called recurrent if it outputs parameters which are used as inputs
the next time the network is evaluated.

## Evolution strategies

Evolution strategies [@visuales] are a black-box optimization technique, they're used to optimize
arbitrary functions given only the ability to evaluate the result. In _Evolution Strategies as a
Scalable Alternative to Reinforcement Learning_ [@openai-es] OpenAI show that evolution strategies
are a competitive alternative to other reinforcement learning techniques: They're easier to implement,
easier to scale, and have fewer hyperparameters.

In an evolution strategy, populations are generated from a multi-variate normal distribution with some
mean and covariance. Each individual is then evaluated, and based on their performance the evolution
strategy updates the mean and covariance such that the next generation is likely to produce
better solutions than the current generation.

The Covariance Matrix Adaptation Evolution Strategy (CMA-ES) [@CMA],[@MullerES],[@Igel09] is
one algorithm for updating the mean & covariance of a population, and is said to be ideally suited to 
neuroevolution: It adapts to badly scaled objective functions (where the effect of some change on one
input variable has a much greater or smaller impact than a change of the same magnitude to another input variable)
by individually controlling the distributions of each variable and the joint variability of each pair
of variables; It doesn't require a large population size, making it efficient to evaluate; and it
prevents premature convergence.
CMA-ES is described briefly below, see [@CMA] for the full algorithm.

### Multivariate normal distribution sampling

A multi-variate normal distribution is a generalization of the normal distribution to $k$ dimensions: 
The mean becomes a vector in $\mathbb{R}^k$, and the variance and correlation are given by a covariance
matrix $K \in \mathbb{R}^{n \times n}$ where $K_{i,j}$ gives the covariance between 
the $i$th and $j$th element of the vector. Intuitively the diagonal of the covariance matrix gives the
variance of each variable.

In evolution strategies we need to sample points from a multi-variate normal distribution.
To do this first the eigendecomposition of the covariance matrix is calculated. A point is then
sampled by sampling the normal distribution with a mean of 0 and variance of 1 for each variable,
giving a uniform normally distributed vector, then this vector is scaled by the eigenvalues and 
rotated by the eigenvectors of the covariance matrix, and the mean is added. This transforms the point into the space
of the distribution.

In CMA-ES, before adding the mean the sampled point is scaled by $\sigma^{(g)}$, the "overall" 
standard deviation, or step-size, which is controlled separately to the covariance.

### Updating the parameters

After the first generation has been sampled and evaluated the next step is to update the mean, 
covariance, and step-size.

The mean is calculated by re-combining the means from a subset of the previous generation and 
weighting them by their fitness. To prevent loosing information when the objective function is
noisy, the mean is updated with exponential decay: the new mean is a linear combination of
the previous mean and the weighted sum of the best individuals.

To update the covariance matrix CMA-ES uses the standard formula for estimating the covariance
from a distribution, but only includes the highest ranked individuals in the calculation. This
skews the distribution towards the values which are performing well. This is known as rank-$\mu$
update, where $\mu$ is the fraction of the population which is included in the calculation.

CMA-ES combines rank-$\mu$ update with rank-$1$ update, where only a single individual is included:
this individual is not taken from the population, but rather is calculated over multiple generations
by summing the changes in the mean: If the mean continues to move in the same direction then
the sum of the mean will have high magnitude, using this sum to update the covariance matrix stretches
the distribution in the direction it's moving, so future generations will take larger steps in that
direction. The summing of the mean is called the evolution path.

Each generation the rank-$\mu$ update, rank-$1$ update, and previous covariance are combined linearly
to get a new covariance matrix to sample from.

The final parameter $\sigma^{(g)}$ is used to scale the overall distribution, in contrast to the
covariance matrix update which scales in a single direction. It's updated using an evolution path,
like in the rank-$1$ covariance matrix update.


# Implementation

To gain a full understanding of the algorithms they were implemented from scratch. 
The pragmatic approach would be to use existing libraries like tensorflow [@tensorflow] and 
PyBrain [@pybrain], which have advantages in both performance [@TensorflowPerf] and development time, 
but that would have been less interesting.

This section gives an overview of what exists at the time of writing, more is said on the 
incremental process of implementation, testing, and debugging in a later section.

## An efficient training environment

The bots are trained in a minimal model of the game rather than the game itself.
Instrumenting the game for training would have been a significant task: To avoid
the performance penalty of running the entire game would require decoupling 
the subset of behaviour which is necessary for simulating just plane dynamics
and collision from the rest. Combined with a lack of familiarity with the game's 
source code it was decided to train the agents externally. 

The training environment is written in rust [@rust] and emulates the plane dynamics 
and collision handling of the game. With the exception of acceleration structures,
algorithms were chosen to match the original game where possible: Collision detection 
is based on the separating axis theorem [@sat] and accelerated with a bounding volume 
tree [^bvt]. The BVT also accelerates ray-casting into the environment, used as 
input for the agents. 

[^bvt]: I recently came across the idea of using the union of
a pair of aligned triangles as a bounding volume, one triangle facing up and the other
down to create a hexagonal volume. _Generating_ this bounding volume for a shape is more 
expensive but the intersection test may be more efficient, it seems an interesting 
candidate for use in a bounding volume tree.

### Evaluating agents

The next objective was to create a scenario with a quantifiable measure of an 
agent's performance where maximising this measure corresponds to a bot which can 
fly well when used in the real game.
To train the agents, first a set of agents are created with a random distribution,
then they play through the scenario and their performance is measured, at the end
of the scenario a new population is generated based on the performance of the 
tested agents. The performance of an agent in a single run of this scenario
should be indicative of performance in the game.

The chosen scenario is collecting a sequence of points placed randomly in a fixed
area of the environment: 

 - Multiple points are required for training methods which discard information from 
   previous runs: The set of agents which can successfully move from one fixed point to 
   another is much larger than the set which have generalized the concept and can continue
   moving to arbitrary targets as their input changes, we want to find the latter.
 - The scenario must have an end condition which doesn't require the bot winning.
   We used a timer. The game ends after a given number of updates, chosen such that an 
   agent can feasibly beat any permutation of the scenario before the time ends, but 
   to otherwise be as short as possible for performance.
 - The environment is also randomized at each stage to avoid over-fitting. Obstacles are 
   placed randomly in an area larger than that which contains the targets, with an exclusion
   zone around the spawning point of the plane to prevent collisions on the first tick.
   The size and orientation of the obstacles are random within a reasonable range.
 
The scenario is parameterised by the number of obstacles and the number of points to collect.
Models can be trained on different parametrisation in sequence. Starting in an environment 
without obstacles was observed to speed up training significantly.

The fitness of an agent is a combination of multiple factors:

 - The number of points collected;
 - How far along the path to the goal it got before the game ended;
 - How many times the plane crashes.

The weighting of each of the factors was tuned by hand. In particular, reducing fitness for
planes which crash helps the agents learn to avoid obstacles, but if it's too highly weighted
the agents learn that it's safer to stay in the center of the map and circle, since there's
no chance of crashing there. Since the fitness measure is independent of time, the game must 
end as soon as a single agent completes the course, otherwise all agents who completed the 
course would have the same fitness regardless of time taken.

### Visualization

The training environment has a simple wireframe visualization, running outside the training 
thread to monitor the progress of the agents. The visualised population follows the
same distribution the CMA-ES samples from. The agents appear as a swarm: In situations 
where the algorithm seems to have converged the swarm is more tightly packed. 
The visualisation also shows the ray-casted input to the agents, which isn't
particularly useful but does make it look more interesting.


## A neural-network based agent

The neural network implementation is minimal and unoptimized. Profiling showed that 
time spent evaluating the model is negligible compared to physics simulations and the
learning algorithms. Techniques such as experience replay [@ExperienceReplay] would shift this balance
and give more value to an optimized implementation of the model.

The network is comprised of a parameter vector and a sequence of layers. Each layer
is a function of a slice of the parameter vector and an input vector and returns a new 
vector, which is used as the input for the next layer. The output of the last layer is the output
of the network. The following layers are implemented:

 - linear layers, in which each output is a linear combinataion of all inputs, with the
   coefficients/weights being read from the parameter vector;
 - bias layers, which add a value to each input;
 - and activation layers, which apply a fixed function to every input. The common functions
   $tanh$ and $max(0)$ are included.

Every frame the agent collects it's input into a vector and feeds it through the network,
the output of the network is then used to set the control state of the plane for that frame.
The agent takes as input the state of the plane and the ray-casted distance to the closest 
obstacle in multiple directions around the plane. All angles are represented as vectors [^angles] 
passed to the agent as $sin(\theta), cos(\theta)$.

A simple recurrent topology was also tested, where the input includes the previous frame's output.
With this topology only a single ray was cast in a direction determined by an extra output parameter,
so the agent decides where to look, and gets the distance to the closest obstacle in that direction
on the next frame. Since the network is recurrent it also get's the direction vector.

The ray-casted distances are clamped to a range about the width of the player's viewport
and normalized. The output is the angle the agent wants to turn and a change in throttle. 
The turn value is read a vector and the angle is reconstructed with $atan2(x, y)$. 

[^angles]: Passing angles directly to the network was observed to be ineffective. The network 
wasn't able to perform modulo arithmetic, so couldn't calculate the difference between
two angles. The vector representation was more effective as long as the linear layers are 
rectified. 
 
The model can be serialized to a binary format to store on disk between program runs and
to be transferred from the training environment to the game.

## Optimization algorithms

The implementations of the optimization algorithms are a direct translation
from the algorithms as described in the papers cited above into code. The Rust library
nalgebra [@nalgebra] was used for vector and matrix operations, it's API is pleasant to use
but the library is still in development and not yet optimized. 
There was no existing implementation in Rust of multivariate normal distribution sampling,
which is required for CMA-ES, so it was was implemented using the eigendecomposition of 
the covariance matrix as described above.

## Injecting the trained model into Altitude

Altitude is written in Java, so the rust implementation of the agent was exported as a DLL
called the model runner, which conforms to the Java Native Interface.
The runner consists of a single function `run: [f32] -> [f32]`, which takes an array
as input, feeds it through a pre-trained neural network, and returns the result.
Altitude was modified to load this DLL and the navigation code was changed to use it.
The modifications are done on the game server, no changes to the client are needed.
Ray-traced input was not implemented in the java-side of the interface, so only simple
models without vision can currently run in the game.


# Results & Conclusions

In open spaces the agents can learn to fly more efficiently than the original algorithm,
but with obstacles they perform significantly worse: Regardless of topology the agents
did not learn to reliably avoid obstacles in the model after ~10 hours of training. 
The agents based on a recurrent network (where the direction of the rays for the next 
frame is an output of the network) appeared to learn nothing about obstacles: the ray 
direction looked random and the agents made no attempts at all to dodge.

This is more than likely an issue with this particular implementation of the algorithms,
rather than evidence of inapplicability of the algorithms to the problem. Without prior 
experience in machine learning, attempts to track down the fault among the many moving 
pieces failed to bear fruit before the due date of this report. Nevertheless, some useful 
information can be gleamed from both the generated data and the fuzzy meta-optimization 
processes we engage in to encourage the networks to do anything at all. 

## Network topology & evolution parameters

In all figures below the x-axis shows training time in seconds and the y-axis shows 
the average fitness of the population where higher is better, a fitness of 1 is optimal
(but impossible to achieve in most permutations of the environment).

Except where otherwise stated the results below were generated with the following parameters:

 - A population size of 50;
 - Low obstacles density;
 - Two hidden layers with 10 nodes per hidden layer and $max(0, x)$ activation;
 - $\tanh(x)$ activation on the output;
 - An initial standard deviation of 3.0.

![The evolution of networks with two hidden layers and variable nodes per layer](images/multi_layer.png){width=60%}
 
When testing networks with two hidden layers, networks with fewer nodes per layer learnt 
more quickly, with the exception of 5 nodes per layer which seems insufficient to solve
this problem. All networks with at least 10 nodes per hidden layer plateaued at a fitness
of $0.5$. Observing the trained agents, they had learnt to fly towards the next node, but
not to avoid any obstacles in the way.

![The evolution of networks with a single hidden layer and variable nodes per layer](images/single_layer.png){width=60%}

Networks with a single hidden layer trained significantly slower than the multi-layer networks.
There is no correlation between the number of neurons in the hidden layer and the performance,
the test with 40 neurons performed the worst out of set, despite being in the middle of the
numbers of neurons.

![The evolution of networks trained with different population sizes](images/population.png){width=60%}

In general higher population sizes resulted in slightly higher performance up to a population of about
$40$.

![The evolution of networks trained in environments with different obstacle densities](images/obstacles.png){width=60%}

The effect of adding more obstacles is as one would expect slower training and lower overall performance.

## The practicalities of the universal approximation theorem

After first implementing the model, a simple neural network, and a weighted evolution
strategy, it was found that linear neural networks can't (easily) learn wrapping arithmetic.
The input was two angles, and a reasonable output would be the difference between them.
The agents learnt $a - b$, but not the slightly more complex angle difference function 
$(a - b) \bmod 2\pi - \pi$.
This was surprising. The universal approximation theorem states that any continuous function
can be approximated with a neural network [@approximation]. Of course, the theorem states 
nothing of the less concrete ability of the user to find a topology which can be trained in a limited time by a 
particular algorithm to approximate a given function.

It was also found that the networks are particularly sensitive to the activation function used:
Switching to the vector representation of directions solved the angle problem, but required
a rectifying ($max(0)$) activation function. The training was much faster if the vectors
were normalized.

## Further work

To end, a few ideas on the directions this project may go in the future:

 - Try evolving the topology of the neural network, as in NEAT [@NEAT];
 - Experiment with different network architectures like LSTM layers;
 - Benchmark the CMA-ES implementation with standard functions, compare 
   the results to those of proven implementations and investigate discrepancies;
 - Optimize the linear algebra & neural network code for faster training (SIMD, reduce allocation, GPGPU, etc);


# References
\small
\setlength{\parindent}{0em}
\setlength{\parskip}{\baselineskip}
\setstretch{1.3}

<div id=refs> </div>
