use esc::render::*;
use esc::resources::*;
use esc::storage::*;
use esc::window::*;
use esc::util::time::*;
use pilot::game::world::*;

fn main() {
   simple_logger::init_with_level(log::Level::Debug).unwrap();
   res::push_source(Directory::open("res"));

   let mut s = Storage::default();

   let surface = s.run(Window::surface);
   let view = s.create();
   s.insert(view, View::fill(1280, 720, surface));
   s.run(|w: &mut Window| w.set_title("pilot"));


   let world = res::get::<Vec<Entity>>("world").unwrap();
   for e in &*world {
      e.reify(&mut s);
   }

   // Everything is components
   // Server sends components
   //  - if entity doesn't exist it's created
   //  - if componment isn't in entity it's added
   //  - otherwise it's updated
   //  - every confirm frame, resimulate if input components
   //    don't match that frame's input state
   //  - Hit events reified
   //  - need to store input for every frame, for deletion of mismatched creates
   //  - Networked component 
   //  - Everything can be re-created from the initial state and list of frame inputs
   //    - or initial state + snapshot + frame
   //  - while resimulating entities created in the future must be hidden/deleted
   //    - deleted: even if *input* state for that particular entity hasn't changed,
   //      something else in the world may change it's *final* state
   //
   //  world contains:
   //    - actual state
   //    - snapshots back to confirm frame
   //    - frame input back to confirm frame
   //    - both frame input & snapshots are just sets of components
   //
   //  pub fn rollback(n: usize) { .. }
   //  pub fn setup_world(world: &mut World, snapshot: Snapshot) { .. }
   //  pub fn hash_game_state(world: &mut World) { .. }
   //  pub fn tick()
   //
   //  - need to be able to join mid-game -> send full snapshot
   //    - snapshot needs to include exact state of everything 
   //      gameplay-effecting -> need to quantize every frame.
   //        - that should also make determinism easier, less drift as unlikely
   //          to be on quant borders.
   //  - frame inputs:
   //     - plane input
   //     - plane spawn
   //     - powerup seed
   //     

   loop {
      s.run(Window::poll_events);
      if s.run(Window::is_close_requested) { break }
      input(&mut s);

      while s.run(GameTime::tick) {
         tick(&mut s);
      }

      draw(&mut s);
      s.run(Window::flip);
   }
}

